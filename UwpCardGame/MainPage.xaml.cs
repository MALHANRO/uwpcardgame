﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UwpCardGame
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Random random = new Random();
        string[] suits = new string[] { "Clubs", "Diamonds", "Hearts", "Spades" };
        string[] values = new string[] { "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "Knight" };
        private int pSuite, rSuite;
        private int pValue, rValue;
        private string file1, file2;
        private bool turnStatus = false;
        private int totalRounds, houseScore=0, playerScore=0;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnPageLoaded(object sender, RoutedEventArgs e)
        {
//Initially the switch and play buttons will be disabled
            BSwitchCards.IsEnabled = false;
            BPlayCards.IsEnabled = false;
//Gives the path for the cards
            ImgPlayerCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/CardBack.png"));
            ImgHouseCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/CardBack.png"));
        }

        private void BDealCards_Click(object sender, RoutedEventArgs e)
        {
// After the Deal button has been clicked, the other two buttons(Play,Switch) will be enabled and Deal button will be disabled
            BSwitchCards.IsEnabled = true;
            BPlayCards.IsEnabled = true;
            BDealCards.IsEnabled = false;
//Generates a random card based on the index
            pSuite = random.Next(0, 4);
            rSuite = random.Next(0, 4);
            pValue = random.Next(1, 14);
            rValue = random.Next(1, 14);

            ImgPlayerCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/CardBack.png"));
            ImgHouseCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/CardBack.png"));
        }

        private void BSwitchCards_Click(object sender, RoutedEventArgs e)
        {
            BDealCards.IsEnabled = false;
//Adds a leading zero to the card number wihich have a one digit(0-9)
            string fileName1 = suits[pSuite].Substring(0, 1) + pValue.ToString().PadLeft(2, '0');
            string fileName2 = suits[rSuite].Substring(0, 1) + rValue.ToString().PadLeft(2, '0');
            turnStatus = true;
            ImgPlayerCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/CardBack.png"));
            ImgHouseCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/CardBack.png"));
        }
        private void BPlayCards_Click(object sender, RoutedEventArgs e)
        {
    //swaps the house and player cards
            string fileName1 = suits[rSuite].Substring(0, 1) + rValue.ToString().PadLeft(2, '0');
            string fileName2 = suits[pSuite].Substring(0, 1) + pValue.ToString().PadLeft(2, '0');
            file1 = fileName1;
            file2 = fileName2;
            BSwitchCards.IsEnabled = false;
            BPlayCards.IsEnabled = false;
            BDealCards.IsEnabled = true;

            if (turnStatus == false)
            {
                ImgPlayerCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/{file2}.png"));
                ImgHouseCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/{file1}.png"));
//evaluates that which player has a larger card and on basis of that scores of either the player or house are incremented by 1
                if (pValue > rValue)
                {
                    TxtScoreBoard.Text = "Great Job!!!,House Wins with " + values[pValue - 1] + " of " + suits[pSuite] + " against " + 
                        values[rValue - 1] + " of " + suits[rSuite];
                    houseScore = houseScore + 1;
                    TxtHouseScore.Text = houseScore.ToString();
                    totalRounds = totalRounds + 1;
                }
                else if (pValue == rValue)
                {
                    TxtScoreBoard.Text = "Ah-Oh!!! It's a Tie";
                    totalRounds = totalRounds+ 1;
                }
                else
                {
                    TxtScoreBoard.Text = "Fantastic, Player Wins with " + values[rValue - 1] + " of " + suits[rSuite] + " against " + values[pValue - 1] + 
                        " of " + suits[pSuite];
                    playerScore = playerScore + 1;
                    TxtPlayerScore.Text = playerScore.ToString();
                    totalRounds = totalRounds + 1;
                }
            }
            else
            {
                ImgPlayerCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/{file2}.png"));
                ImgHouseCard.Source = new BitmapImage(new Uri($"ms-appx:///Assets/CardImages/{file1}.png"));

                if (pValue > rValue)
                {
                    TxtScoreBoard.Text = "Amazing, Player Wins with " + values[pValue - 1] + " of " + suits[pSuite] + " against " + values[rValue - 1] + 
                        " of " + suits[rSuite];
                    playerScore = playerScore + 1;
                    TxtPlayerScore.Text = playerScore.ToString();
                    totalRounds =totalRounds + 1;
                }
                else if (pValue == rValue)
                {
                    TxtScoreBoard.Text = "Ah-Oh!!! It's a Tie";
                   totalRounds = totalRounds + 1;
                }
                else
                {
                    TxtScoreBoard.Text = "Excellent, House Wins with " + values[rValue - 1] + " of " + suits[rSuite] + " against " + values[pValue - 1] + 
                        " of " + suits[pSuite];
                    houseScore = houseScore + 1;
                    TxtHouseScore.Text = houseScore.ToString();
                   totalRounds = totalRounds + 1;
                }
                // To check for the maximum number of rounds allowed and then announcing the winnner 
                if (totalRounds==5)
                {
                // All the buttons are disabled when the maximum rounds allowed have been completed 
                    BSwitchCards.IsEnabled = false;
                    BPlayCards.IsEnabled = false;
                    BDealCards.IsEnabled = false;
                    if (playerScore < houseScore)
                    {
                        TxtScoreBoard.Text = "            ****GAME OVER*****\n" +
                            "Congratulations!!! House Wins with " + houseScore.ToString() + " points"; 
                    }
                    else if (houseScore == playerScore)
                    {
                        TxtScoreBoard.Text = "    ******GAME OVER*****\n" +
                            " The Score Is Even";
                    }
                    else
                    {
                        TxtScoreBoard.Text = "           ****GAME OVER***** \n" +
                            "Congratulations!!! Player Wins with " + playerScore.ToString() + " points"; 
                    }
                }
            }
        }
    }
}
